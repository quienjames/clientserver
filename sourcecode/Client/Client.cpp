// ConsoleApplication2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <string>
#include <iostream>
#include <windows.h> 
#include <stdio.h> 
#include <tchar.h>
#include <strsafe.h>
#include <thread>
#include <mutex>

#include "json.hpp"
#include "..\..\ThreadPool\ThreadPool.h"

#define PIPENAME "\\\\.\\pipe\\Pipe"
BOOL   fSuccess = FALSE;
#define BUFSIZE 1024

std::mutex mAllowedToPrint;
std::mutex mMessageExchange;
int iMessagesToReceive = 0;
bool bAlreadyPulsing = false;

void printForThread(std::string sInput, int iThreadID){
	mAllowedToPrint.lock();
	if (iThreadID >= 0) {
		std::cout << "ID:" << iThreadID << " - ";
	}
	if (!sInput.empty()) {
		std::cout << sInput << std::endl;
	}
	mAllowedToPrint.unlock();
}

int InstanceThread(int iThreadID, std::string sInput) {
	mMessageExchange.lock();

	char  chBuf[BUFSIZE];
	HANDLE hPipe;
	DWORD dwWritten, cbRead, cbToWrite, cbWritten, dwMode;
	std::stringstream ssMessage;
	json::document jDoc;
	jDoc.parse(sInput);
	jDoc["ID"] = iThreadID;

	int iWaitTwentySeconds = 0;

	while (1) {
		hPipe = CreateFile(
			TEXT(PIPENAME),
			GENERIC_READ | GENERIC_WRITE,
			0, 
			NULL,
			OPEN_EXISTING,
			0,
			NULL);

		if (hPipe != INVALID_HANDLE_VALUE) {
			break;
		}
		if (GetLastError() != ERROR_PIPE_BUSY) {
			ssMessage.str("");
			ssMessage << "Could not open pipe. GLE=" << GetLastError();
			printForThread(ssMessage.str(), iThreadID);
			mMessageExchange.unlock();
			return -1;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		// All pipe instances are busy, so wait for 20 seconds. 
		if (iWaitTwentySeconds < 20) {
			iWaitTwentySeconds++;
		} else {
			mMessageExchange.unlock();
			return -1;
		}
	}

	if (sInput != "") {
		fSuccess = WriteFile(
			hPipe,
			jDoc.write().c_str(),
			BUFSIZE * sizeof(char),
			&dwWritten,
			NULL);
	} else {
		fSuccess = WriteFile(
			hPipe,
			sInput.c_str(),
			BUFSIZE * sizeof(char),
			&dwWritten,
			NULL); 
	}

	if (!fSuccess)
	{
		ssMessage.str("");
		ssMessage << "WriteFile to pipe failed. GLE=" << GetLastError();
		printForThread(ssMessage.str(), iThreadID);
		mMessageExchange.unlock();
		return -1;
	}
	if (!sInput.empty()) {
		printForThread("Message sent to server - " + sInput, iThreadID);
		iMessagesToReceive++;
	}

	do
	{
		// Read from the pipe. 
		fSuccess = ReadFile(
			hPipe,    // pipe handle 
			chBuf,    // buffer to receive reply 
			sizeof(chBuf) - 1, 
			&cbRead,  // number of bytes read 
			NULL);    // not overlapped 
		chBuf[cbRead] = '\0';

		if (!fSuccess && GetLastError() != ERROR_MORE_DATA) {
			break;
		}
	} while (!fSuccess);  // repeat loop if ERROR_MORE_DATA 

	if (!fSuccess){
		ssMessage.str("");
		ssMessage << "ReadFile to pipe failed. GLE=" << GetLastError();
		printForThread(ssMessage.str(), -1);
		mMessageExchange.unlock();
		return -1;
	} else {
		if (strlen(chBuf) > 0) {
			printForThread(std::string(chBuf) + " - Message received from server", -1);
			iMessagesToReceive--;
		}
	}
	CloseHandle(hPipe);

	mMessageExchange.unlock();
}

void PulseThread(void) {
	while (1) {
		mMessageExchange.lock();
		if (iMessagesToReceive > 0) {
			mMessageExchange.unlock();
			InstanceThread(-1, "");
		} else {
			mMessageExchange.unlock();
		}
	}
}

int main(void) {
	std::string sInput;
	std::stringstream ssMessage;	
	int iThreadID = 1;
	json::document jDoc;
	HANDLE hThread = NULL;
	DWORD  dwThreadId = 0;

	ThreadPool thpPool(8);
	thpPool.enqueue(PulseThread);
	while (1){
		std::getline(std::cin, sInput);

		if (jDoc.parse(sInput)) {

			auto result = thpPool.enqueue(InstanceThread, iThreadID, jDoc.write());
			iThreadID++;
		} else {
			printForThread("Invalid json input", iThreadID);
		}
	}
	
	return (0);
}