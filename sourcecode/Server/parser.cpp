#include "pch.h"
#include "parser.hpp"
#include "clientdefinedclasses.hpp"

extern ThreadPool 			ThPoolApplyCommand(8);

Parser::Parser(void) {
	;
}

void Parser::storeData(){
	mDatabase.lock();
	jStoredData.writeFile(STOREDDATA, true);
	mDatabase.unlock();
}

void Parser::retrieveData(){
	mDatabase.lock();
	if (!jStoredData.parseFile(STOREDDATA)){
		jStoredData.emptyObject();
		jStoredData.writeFile(STOREDDATA, true);
	}
	mDatabase.unlock();
} 

std::string sPutIDNumber(std::string sOutputRaw, int iIDNumber) {
	json::document jAddNumber;
	return "ID: " + std::to_string(iIDNumber) + " " + sOutputRaw;	
}

void Parser::helperApplyCommand(std::string sInput) {
	json::document jDoc;
	std::cout << "Raw Input: " << sInput << " - end" << std::endl;
	mResponses.lock();
	std::this_thread::sleep_for(std::chrono::seconds(3));
	if (jDoc.parse(sInput)) {
		int iCase = checkType(jDoc["command"].string());
		// read was successful
		switch(iCase){
			case 0:
				jResponses["Responses"].push_front(sPutIDNumber(getValue(jDoc), jDoc["ID"].number()));
				break;
			case 1:
				jResponses["Responses"].push_front(sPutIDNumber(postValue(jDoc), jDoc["ID"].number()));
				break;
			case 2:
				jResponses["Responses"].push_front(sPutIDNumber(deleteValue(jDoc), jDoc["ID"].number()));
				break;
			case 3:
				jResponses["Responses"].push_front(sPutIDNumber(defValue(jDoc), jDoc["ID"].number()));
				break;
			case 4:
				{
					ClientDefinedClasses cldc;
					jResponses["Responses"].push_front(sPutIDNumber(cldc.funcValue(jDoc, jStoredData), jDoc["ID"].number()));
				}
				break;
			default:
				jResponses["Responses"].push_front("Invalid command ID: " + jDoc["ID"].string());
		}
		mResponses.unlock();
		return;
	}
	jResponses["Responses"].push_front("Invalid command ID: " + jDoc["ID"].string());
	mResponses.unlock();
	return;
}

std::string Parser::applyCommand(std::string sInput) {

	std::string sResponse = "";
	mResponses.lock();
	if (sInput != "") {
		ThPoolApplyCommand.enqueue(&Parser::helperApplyCommand, this, sInput);
	}
	
	if (jResponses.exists("Responses") && !jResponses.empty()) {
		sResponse = jResponses["Responses"].pop_back().string();
		//		std::thread ThPoolApplyCommand(&Parser::helperApplyCommand, this, sInput);
	} else {
		jResponses["Responses"].emptyArray();
	}
	mResponses.unlock();
	return sResponse;

}

int Parser::checkType(std::string sInputType){
	if(sInputType == "GET"){
		return 0;
	} else if(sInputType == "POST"){
		return 1;
	} else if(sInputType == "DELETE"){
		return 2;
	} else if(sInputType == "DEFINE_CLASS"){
		return 3;
	} else if(sInputType == "FUNCTION"){
		return 4;
	} else{
		return -1;
	}
}

std::string Parser::getValue(json::document jInput){
	std::string sKey = jInput["Key"].string();
	if (jStoredData.exists(sKey)){
		return jStoredData[sKey].string();
	} else {
		return "Key invalid";
	}
}

std::string Parser::postValue(json::document jInput){
	std::string sKey = jInput["Key"].string();
	jStoredData[sKey] = jInput["Value"];
	storeData();
	return sKey + ":" + jStoredData[sKey].string();
}

std::string Parser::deleteValue(json::document jInput){
	std::string sKey = jInput["Key"].string();
	if (jStoredData.exists(sKey)){
		jStoredData[sKey].destroy();
		storeData();
	} else {
		return "Key invalid";
	}
	return sKey + " Deleted";
}

std::string Parser::defValue(json::document jInput){
	std::string sKey = jInput["Key"].string();
	jStoredData["__class_definition__"][sKey] = jInput["Value"];
	storeData();
	return sKey + ":" + jStoredData["__class_definition__"][sKey].string();
}