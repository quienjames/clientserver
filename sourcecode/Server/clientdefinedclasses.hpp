#define STOREDDATA 		"stored_data.json"
#include <mutex>
#include "json.hpp"
extern std::mutex mDatabase;

class ClientDefinedClasses{
public:
	std::string 		funcGetEntireClassValue(json::document, json::document),
						funcPostEntireClassValue(json::document, json::document),
						funcGetValue(json::document, json::document),
						funcPostValue(json::document, json::document),
						funcAddValue(json::document, json::document),
						funcAppendValue(json::document, json::document),
						funcValue(json::document, json::document);
	static int 			functionType(std::string);
	bool 				validateClass(json::document, json::document);
};
