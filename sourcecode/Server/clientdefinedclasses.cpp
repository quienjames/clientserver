#include "pch.h"
#include "clientdefinedclasses.hpp"
#include "json.hpp"
#include <string>
#include <mutex>

int ClientDefinedClasses::functionType(std::string sInputType) {
	if (sInputType == "GET_WHOLE") {
		return 0;
	} else if (sInputType == "POST_WHOLE") {
		return 1;
	} else if (sInputType == "GET") {
		return 2;
	} else if (sInputType == "POST") {
		return 3;
	} else if (sInputType == "SUM") {
		return 4;
	} else if (sInputType == "APPEND") {
		return 5;
	} else {
		return -1;
	}
}

std::string ClientDefinedClasses::funcValue(json::document jInput, json::document jServerStoredData){
	//check class definition
	if (jServerStoredData["__class_definition__"].exists(jInput["Value"]["classType"].string())) {
		int iFunctionCase = functionType(jInput["classFunction"].string());
		// read was successful
		switch (iFunctionCase) {
			case 0:
				return funcGetEntireClassValue(jInput, jServerStoredData);
			case 1:
				return funcPostEntireClassValue(jInput, jServerStoredData);
			case 2:
				return funcGetValue(jInput, jServerStoredData);
			case 3:
				return funcPostValue(jInput, jServerStoredData);
			case 4:
				return funcAddValue(jInput, jServerStoredData);
			case 5:
				return funcAppendValue(jInput, jServerStoredData);
			default:
				return "Invalid classFunction value";
		}
	}
	// sKey = jInput["Key"].string();
	// sValue = jInput["Value"].string();
	return "";
}

bool ClientDefinedClasses::validateClass(json::document jInput, json::document jServerStoredData){
	std::string sClassType = jInput["Value"]["classType"].string();
	if (!jServerStoredData["__class_definition__"].exists(sClassType)){
		return false;
	}

	//Check that there are no missing attributes
	for(auto aAttribute = jServerStoredData["__class_definition__"][sClassType]["attributes"].begin(); 
		aAttribute != jServerStoredData["__class_definition__"][sClassType]["attributes"].end(); ++aAttribute){
	    if (!jInput["Value"].exists((*aAttribute).string())){
	    	if ((*aAttribute).key() != "classType"){
	    		return false;
	    	}
	    }
	}
	return true;
}


std::string ClientDefinedClasses::funcGetEntireClassValue(json::document jInput, json::document jServerStoredData){
	std::string sKey = jInput["Key"].string();
	if (jServerStoredData.exists(sKey)){
		json::document jTemp = jServerStoredData[sKey];
		return jTemp.write();
	} else {
		return "Key invalid";
	}
}

std::string ClientDefinedClasses::funcPostEntireClassValue(json::document jInput, json::document jServerStoredData){
	//todo::add a check to this

	if (validateClass(jInput, jServerStoredData)){
		std::string sKey = jInput["Key"].string();
		jServerStoredData[sKey] = jInput["Value"];
		mDatabase.lock();
		jServerStoredData.writeFile(STOREDDATA, true);
		mDatabase.unlock();
		json::document jTemp = jServerStoredData[sKey];
		return sKey + ":" + jTemp.write();
	} else {
		return "Class Definition invalid";
	}
}

std::string ClientDefinedClasses::funcGetValue(json::document jInput, json::document jServerStoredData){
	std::string sKey = jInput["Key"].string();
	std::string sClassType = jServerStoredData[sKey]["classType"].string();
	json::document jReturn;
	jReturn[sKey].emptyObject();

	for(auto aAttribute = jInput["attributes"].begin(); aAttribute != jInput["attributes"].end(); ++aAttribute){
		if (jServerStoredData[sKey].exists((*aAttribute).string())){
			jReturn[sKey][(*aAttribute).string()] = jServerStoredData[sKey][(*aAttribute).string()];
		} else {
			return "Key: " + (*aAttribute).string() + "invalid";
		}
	}
	std::string sResponse = jReturn.write();
	return sResponse;

}

std::string ClientDefinedClasses::funcPostValue(json::document jInput, json::document jServerStoredData){
	std::string sKey = jInput["Key"].string();
	std::string sClassType = jServerStoredData[sKey]["classType"].string();

	for (auto aAttribute = jInput["attributes"].begin(); aAttribute != jInput["attributes"].end(); ++aAttribute) {
		if (!jServerStoredData[sKey].exists((*aAttribute).key())) {
			return "Key: " + (*aAttribute).string() + "invalid";
		}
	}

	for(auto aAttribute = jInput["attributes"].begin(); aAttribute != jInput["attributes"].end(); ++aAttribute){
		jServerStoredData[sKey][(*aAttribute).key()] = (*aAttribute);
	}

	mDatabase.lock();
	jServerStoredData.writeFile(STOREDDATA, true);
	mDatabase.unlock();

	json::document jTemp = jServerStoredData[sKey];
	return jTemp.write();
}

std::string ClientDefinedClasses::funcAddValue(json::document jInput, json::document jServerStoredData){
	std::string sKey = jInput["Key"].string();
	std::string sClassType = jServerStoredData[sKey]["classType"].string();
	double fSum = 0.0;

	bool bSumFound = false;
	for (auto aAttribute = jServerStoredData["__class_definition__"][sClassType]["functions"].begin(); \
		aAttribute != jServerStoredData["__class_definition__"][sClassType]["functions"].end();\
		++aAttribute) {
		if ((*aAttribute).string() == "SUM"){
			bSumFound = true;
		}
	}

	if (!bSumFound){
		return "SUM not in Class Definition of " + sClassType;
	}
	
	for(auto aAttribute = jInput["attributes"].begin(); aAttribute != jInput["attributes"].end(); ++aAttribute){
		if (((jServerStoredData[sKey][(*aAttribute).string()].isA() != json::JSON_NUMBER) && \
			(jServerStoredData[sKey][(*aAttribute).string()].isA() != json::JSON_STRING)) || \
			!jServerStoredData[sKey].exists((*aAttribute).string())){
			return "Key-Value: " + (*aAttribute).string() + "invalid";
		} else {
			fSum += jServerStoredData[sKey][(*aAttribute).string()].number();
		}
	}
	return std::to_string(fSum);
}


std::string ClientDefinedClasses::funcAppendValue(json::document jInput, json::document jServerStoredData){
	std::string sKey = jInput["Key"].string();
	std::string sClassType = jServerStoredData[sKey]["classType"].string();
	std::string sConcatenatedValue = "";

	bool bAppendFound = false;
	for (auto aAttribute = jServerStoredData["__class_definition__"][sClassType]["functions"].begin(); \
		aAttribute != jServerStoredData["__class_definition__"][sClassType]["functions"].end(); \
		++aAttribute) {
		if ((*aAttribute).string() == "APPEND"){
			bAppendFound = true;
		}
	}

	if (!bAppendFound){
		return "APPEND not in Class Definition of " + sClassType;
	}

	for (auto aAttribute = jInput["attributes"].begin(); aAttribute != jInput["attributes"].end(); ++aAttribute) {
		if (((jServerStoredData[sKey][(*aAttribute).string()].isA() != json::JSON_NUMBER) && \
			(jServerStoredData[sKey][(*aAttribute).string()].isA() != json::JSON_STRING)) || \
			!jServerStoredData[sKey].exists((*aAttribute).string())) {
			return "Key-Value: " + (*aAttribute).string() + "invalid";
		} else {
			sConcatenatedValue += jServerStoredData[sKey][(*aAttribute).string()].string();
		}
	}

	return sConcatenatedValue;
}