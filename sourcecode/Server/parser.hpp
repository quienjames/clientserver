#include "json.hpp"
#include "../../ThreadPool/ThreadPool.h"
#include <mutex>
#include <iostream>


extern json::document		jStoredData;
extern json::document		jResponses;
extern std::mutex 			mResponses;


class Parser {
public:
	Parser();
	std::string			applyCommand(std::string);
	int 				checkType(std::string);
	void				storeData(),
						retrieveData();
private:
	void				helperApplyCommand(std::string);
	std::string 		getValue(json::document),
						postValue(json::document),
						deleteValue(json::document),
						defValue(json::document);
};