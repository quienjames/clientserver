#include "pch.h"
#include <iostream>
#include <windows.h> 
#include <stdio.h> 
#include <tchar.h>
#include <strsafe.h>
#include <mutex>

#include "clientdefinedclasses.hpp"
#include "parser.hpp"

#include "../../json/json.hpp"
#include "..\..\ThreadPool\ThreadPool.h"

#define PIPENAME 		"\\\\.\\pipe\\Pipe"

BOOL   fSuccess = FALSE;
#define BUFSIZE 1024

json::document jResponses;
std::mutex mResponses;

std::mutex mAllowedToPrint;
json::document		jStoredData;

void printForThread(std::string sInput, int _iThreadID){
	mAllowedToPrint.lock();
	std::cout << sInput + "\n" << std::endl;
	mAllowedToPrint.unlock();
}

std::mutex mDatabase;

DWORD WINAPI InstanceThread(LPVOID);

int main(void)
{
	BOOL   fConnected = FALSE;
	DWORD  dwThreadId = 0;
	HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
	std::stringstream ssMessage;

	for (;;) {
		hPipe = CreateNamedPipe(TEXT(PIPENAME),
			PIPE_ACCESS_DUPLEX,
			PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
			1,
			BUFSIZE * 16,
			BUFSIZE * 16,
			NMPWAIT_USE_DEFAULT_WAIT,
			NULL);

		fConnected = ConnectNamedPipe(hPipe, NULL) ? 
			TRUE : (GetLastError() == ERROR_PIPE_CONNECTED); 

		if (fConnected) {
			 hThread = CreateThread( 
			    NULL,              // no security attribute 
			    0,                 // default stack size 
			    InstanceThread,    // thread proc
			    (LPVOID) hPipe,    // thread parameter 
			    0,                 // not suspended 
			    &dwThreadId);      // returns thread ID 
			if (hThread == NULL) {
				ssMessage.str("");
				ssMessage << "CreateThread failed, GLE=" << GetLastError();
				printForThread(ssMessage.str(), -1);
			    return -1;
			} else {
			 	CloseHandle(hThread); 
			} 
		} else { 
        // The client could not connect, so close the pipe. 
         CloseHandle(hPipe); 
     	}
    } 

	return 0;
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
	//todo::allocate all these variables to the heap
	std::string sResponse;
	std::stringstream ssMessage;
	int iThreadID = -1;
	json::document jDoc;

	//char* chBuffer = (char*)HeapAlloc(hHeap, 0, BUFSIZE * sizeof(char));
	//TCHAR* chBuffer = (TCHAR*)HeapAlloc(hHeap, 0, BUFSIZE * sizeof(TCHAR));

	char*  chBuffer = (char*)malloc(BUFSIZE * sizeof(char));
	DWORD dwRead, cbWritten;
	HANDLE hPipe = NULL;



	if (lpvParam == NULL){
		printForThread("\nERROR - Pipe Server Failure:\n   InstanceThread got an unexpected NULL value in lpvParam.\nInstanceThread exitting.\n", iThreadID);
		if (chBuffer != NULL) {
			free(chBuffer);
		}
		return (DWORD)-1;
	}

	if (chBuffer == NULL){
		printForThread("\nERROR - Pipe Server Failure:\n   InstanceThread got an unexpected NULL heap allocation.\nInstanceThread exitting.\n", iThreadID);
		return (DWORD)-1;
	}

	hPipe = (HANDLE)lpvParam;

	//_tprintf(TEXT("\nPipe Server: Main thread awaiting client connection on %s\n"), TEXT(PIPENAME));
	while (hPipe != INVALID_HANDLE_VALUE) {
		fSuccess = ReadFile(
			hPipe,
			chBuffer,
			BUFSIZE,
			&dwRead,
			NULL);
		/* add terminating zero */
		chBuffer[BUFSIZE -1] = '\0';
		/* do something with data in chBuffer */
		//printf("%s", chBuffer);
		jDoc.parse(std::string(chBuffer));
		if (!fSuccess) {
			if (GetLastError() == ERROR_BROKEN_PIPE) {
				ssMessage.str("");
				ssMessage << "InstanceThread: client disconnected.";
				printForThread(ssMessage.str(), jDoc["ID"].integer());
			} else {
				ssMessage.str("");
				ssMessage << "InstanceThread ReadFile failed, GLE=" << GetLastError();
				printForThread(ssMessage.str(), jDoc["ID"].integer());
			}
			break;
		}

		Parser parser;
		parser.retrieveData();
		sResponse = parser.applyCommand(chBuffer);

		printForThread(sResponse, jDoc["ID"].integer());
		
		fSuccess = WriteFile(
				hPipe,        // handle to pipe 
				sResponse.c_str(),     // chBuffer to write from 
				BUFSIZE, // number of bytes to write 
				&cbWritten,   // number of bytes written 
				NULL);        // not overlapped I/O 

		if (!fSuccess) {
			ssMessage.str("");
			ssMessage << "InstanceThread WriteFile failed, GLE=" << GetLastError();
			printForThread(ssMessage.str(), iThreadID);
			break;
		}

		//FlushFileBuffers(hPipe);
		//DisconnectNamedPipe(hPipe);
	}

	free(chBuffer);

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);

	return 1;
}
