# ClientServer

To see examples of how to send commands to the Server from the Client see "commands.txt" and use the single-line commands. Below these example commands are neater more human-readable versions. These "pretty" versions are for visualization and are not valid for demo, the single-line version should be used.

Functionality:

Client:

Client loops for inputs from the user, which are then sent to a thread to carry out the input to the Server. The InstanceThread function locks the creation of the pipe file, sends the user input to the server, and then receives a response from the server. For sending to the server, the pipe waits for 20 seconds total if it's busy. The sending and receiving functionality is based on the MSDN documention https://docs.microsoft.com/en-us/windows/desktop/ipc/named-pipe-client. On succesful sends and receive the Client keeps track of the number of messages it is still expecting. If there are still messages to receive a seperate PulseThread sends an empty message to the Server, the Server disregards this, and if it has the response ready it sends it back to the Client. (The reasoning for this pulse thread is due to familiarity of me, James Quien, as the protocol for many hardware devices that I currently work on have this sort of "pulse" sent back and forth between Client and Device). If there are no pending messages to receive, the "pulse" stops. Both the PulseThread and InstanceThread have a lock around their send-receive operations as to not cause a race condition. While each thread is sending and receiving messages, the Client itself can continue to receive input from the user. 

Server:

Server loops for inputs from Client and sends back responses for previous commands. When receiving an input, it creates a thread IsntanceThread to carry out the operation, then it checks a queue of responses jResponses["Responses"] for the results of finished operations. If there is one, it sends it, otherwise it resumes waiting for more requests from the client. When it receives an empty message or "pulse" it checks the queue jResponses["Responses"] and sends one if it is available. The server can service multiple clients and its communication is based on the MSDN example https://docs.microsoft.com/en-us/windows/desktop/ipc/multithreaded-pipe-server. Values sent by Client to be stores by Server is saved under "stored_data.json". 

Parser:

A parser class is dedicated to parsing commands from the client and creating responses. 

ClientDefinedClasses:

A clientdefinedclasses class is dedicated to storing and checking objects of a previously defined class from the client. This is to meet requirements for client defined objects with attributes and functions.

Protocol:

See commands.txt for examples of each command the Client can send to the Server via JSON format
If a command fails the server returns an error message indicating the nature of the error - either a missing key value or it asks for attributes not associated with previous Client-defined classes.


Post:

{"command":"POST", "Key":"SomeKey", "Value":"SomeValue"}

Stores a value of "SomeValue" under a key "SomeKey" at the Server. 

Get:

{"command":"GET", "Key":"SomeKey"}

Returns value stored at "SomeKey" 

Delete:

{"command":"DELETE", "Key":"SomeKey"}

Deletes key-value pair stored at "SomeKey"

CLASS FUNCTIONS:

Define class:

{"command":"DEFINE_CLASS", "Key":"SomeClass", "Value":{"attributes":["attribute1", "attribute2"],"functions":["SUM", "APPEND"]}}

By calling this function a class "SomeClass" is defined with attributes and functions.

Example:

	"__class_definition__": {
		"SomeClass": {
			"attributes": [
				"attribute1",
				"attribute2"
			],
			"functions": [
				"SUM",
				"APPEND"
			]
		}
	}

The Client can make calls to retrieve values of certain attributes, call the "SUM" or "APPEND" function (IF they have been defined in the class definition!) and store objects of this class type.

The command "FUNCTION" determines that the command is class specific to a client-defined class along with a class_function specifying what to do in regards to that class.

Post Object of Class:

{"command":"FUNCTION","classFunction":"POST_WHOLE","Key":"Example","Value":{"attribute1":"Made in 1994","attribute2":"Owl Records","classType":"SomeClass"}}

Client stores a value of "Example" of class "SomeClass" with attributes defined to certain values. The clientdefinedclasses class is dedicated to determining if the call has all the correct attributes associated with the class "Example Class", if everything is correct this object is stored.

Get Object of Class:

{"command":"FUNCTION","Key":"Example","classFunction":"GET_WHOLE","Value":{"classType":"SomeClass"}}

Retrieves object of a certain class. Functionally the same as GET.

Post Attributes of Class:

{"command":"FUNCTION","Key":"Example","classFunction":"POST","attributes":{"attribute1":"BlankValue"},"Value":{"classType":"SomeClass"}}

Stores "BlankValue" in attribute "attribute1" at key "Example" of class "SomeClass". The key "attributes" can be populated with key-value pairs for some or all attributes of the "SomeClass" class similar to Post Object of Class. If an attribute is invalid an error message is returned. 

Get Attributes of Class:

{"command":"FUNCTION","Key":"Example","classFunction":"GET","attributes":["attribute1","attribute2"],"Value":{"classType":"SomeClass"}}

Retrieves values of each attribute listed in "attributes", the response is a JSON. If an attribute is invalid an error message is returned. 

Sum Attributes of Class:

{"command":"FUNCTION","Key":"Example","classFunction":"SUM","attributes":["attribute1","attribute2"],"Value":{"classType":"SomeClass"}}

If "SUM" is defined in the class "SomeClass", a user can call this to return the sum of all attributes listed in "attributes", this skips over values that are strings.

Sum Attributes of Class:

{"command":"FUNCTION","Key":"Example","classFunction":"APPEND","attributes":["attribute1","attribute2"],"Value":{"classType":"SomeClass"}}

If "APPEND" is defined in the classes functions, a user can call this to return a string appending the values of all of attributes listed in "attributes", this will also work for numerical values, which will be converted to strings accordingly.


Implementation of requirements:

1. Communication between Server and Client is through Named Pipes - basis of this software is from the MSDN documentation https://docs.microsoft.com/en-us/windows/desktop/ipc/multithreaded-pipe-server for the server and https://docs.microsoft.com/en-us/windows/desktop/ipc/named-pipe-client for the Client

2. Client loops for inputs from the user, which are then sent to thread to carry out the input to the Server. While this is occuring more inputs can be taken. The Server calls a sleep function to emulate processing time to carry out requests from the Client

3. Function calls and storing data fulfill this requirement

4. Previous protocol and example calls in commands.txt show how to create previously defined objects

5. Server loops for inputs from Client and sends back responses for previous commands. When receiving an input it creates a thread to carry out the operation, then stores the response to be sent later. In the meantime it is able to take more requests as they come from the Client.

6. Protocol in the commands.txt shows how to store ("POST") variables in Server, which is stored as a human-readable JSON

7. Protocol in the commands.txt shows how to register a custom class, its functions, attributes, how to retrieve these attributes and how to call methods on them

8. Custom objects are stored in JSON stored_data.json

Third Party Libraries:

JSON parsing, writing and access library: https://github.com/odhinnsrunes/json

Threadpool library: https://github.com/progschj/ThreadPool